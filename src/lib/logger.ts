import { createLogger, format, Logger, transports } from "winston";

const { combine, timestamp, label, printf } = format;

export default function loggerFactory(logLabel: string, level?: string): Logger {
    const customFormat = printf(({ level, message, label, timestamp }) => {
        return `${timestamp} [${label}] ${level}: ${message}`;
    });

    const logger = createLogger({
        level: level ?? "info",
        format: combine(
            label({ label: logLabel }),
            timestamp(),
            customFormat
        ),
        transports: [
            new transports.Console()
        ]
    });

    return logger;
}